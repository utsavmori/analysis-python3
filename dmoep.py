import json
import threading
import time
import os
import makepart

print ("Processing...")
makepart.dopartition()
lock=0
revcount=[]
rate=[]
asinlst=[]
t=time.time()
def getddata(pr):
	fo = open("data"+str(pr), "r")
	c=0
	global lock
	asinlstl=[]
	revcountl=[]
	ratel=[]
	global revcount
	global rate
	global asinlst
	while True:
	    line = fo.readline()
	    if not line: 
	        break
	    else:
	    	jdat=json.loads(line)
	    	asin=jdat["asin"]
	    	if asin in asinlstl:
	    		ind=asinlstl.index(asin)
	    		revcountl[ind]+=1
	    		ratel[ind]=ratel[ind]+jdat["overall"]
	    	else:
	    		asinlstl.append(asin)
	    		revcountl.append(1)
	    		ratel.append(jdat["overall"])

	fo.close()
	
	while True:
		if (lock==1):
			time.sleep(1)#1 sec of sleep while other is writing data to main table
		else:
			lock=1
			for i in range(0,len(asinlstl)):
				if asinlstl[i] in asinlst:
					ind=asinlst.index(asinlstl[i])
					rate[ind]=rate[ind]+ratel[i]
					revcount[ind]=revcount[ind]+revcountl[i]
				else:
					asinlst.append(asinlstl[i])
					rate.append(ratel[i])
					revcount.append(revcountl[i])
			lock=0
			break


t1=threading.Thread(target=getddata,args=(1, ))
t2=threading.Thread(target=getddata,args=(2, ))
t3=threading.Thread(target=getddata,args=(3, ))
t4=threading.Thread(target=getddata,args=(4, ))
t1.start()
t2.start()
t3.start()
t4.start()

t1.join()
t2.join()
t3.join()
t4.join()
ans=open("analysis.csv","w+")
ans.write("ASIN,Average Review,People Reviewed,Decision")
for i in range(0,len(revcount)):
		temp=rate[i]/revcount[i]
			
		if (temp>=3 and temp<4):
			ans.write("\n"+str(asinlst[i])+","+str(temp)+","+str(revcount[i])+",is good")
		elif(temp>=4):
			ans.write("\n"+str(asinlst[i])+","+str(temp)+","+str(revcount[i])+",is very good")
			
		else:
			ans.write("\n"+str(asinlst[i])+","+str(temp)+","+str(revcount[i])+",is not liked by people")
os.remove("data1")
os.remove("data2")
os.remove("data3")
os.remove("data4")

print ('Completed in ',time.time()-t,' Seconds')
print ("---Done---")