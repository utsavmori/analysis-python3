def dopartition():
	file=open('data',"r")
	num_lines = sum(1 for line in open('data'))
	eachfile=(num_lines//4)+1 #make 4 partitions by lines
	for i in range(1,5):
		c=0
		f=open('data'+str(i),"w+")
		while True:
			line=file.readline()
			if not line:
				break
			else:
				c+=1
				f.write(line)
				if (c ==eachfile):
					break
	return